#!/bin/bash

if !(./applyPatches.sh); then
    echo "Could not apply patches!"
    exit 1
else
    ./compile.sh
fi
