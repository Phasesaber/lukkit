Lukkit
===========

A fork of SportBukkit for The Eagle's Nest.

This contains patches that make Bukkit & CraftBukkit better, by including new features, fixing bugs, and making life awesome.

How To
------

Initalize the Bukkit and Craftbukkit modules: `./init.sh`

Apply the patches: `./applyPatches.sh`

Compile Lukkit: `./compile.sh`

The Lukkit binary will be located in build/CraftBukkit/target.
